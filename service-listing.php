<?php include 'header.php';?>


<section class="container-fluid inner-banner">
    <div class="row align-items-center text-center">
        <div class="inner-banner-img-wrap">
            <img src="https://images.unsplash.com/photo-1511578314322-379afb476865?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1169&q=80" alt="">
        </div>
        <h2>Services</h2>
    </div>
</section>

<section class="container-fluid serv-listig my-5">
    <div class="container">
        <div class="row justify-content-center">

            <div class="col-12 col-md-6 col-lg-4">
                <a href="service-inner-1.php" class="single-exp-wrap">
                    <b></b>
                    <img src="https://images.unsplash.com/photo-1505664194779-8beaceb93744?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80" alt="">
                    <div class="exp-cnt">
                        <span>
                            <img src="img/exp-icon-1.png" alt="">
                        </span>
                        <label>Litigation</label>
                        <p>We approach debt recovery issues without threatening clients’ existing business relations, time and money...</p>
                        <strong class="read-more-btn">View More <i class="ms-2 bi bi-arrow-right"></i></strong>
                    </div>
                </a>
            </div>

            <div class="col-12 col-md-6 col-lg-4">
                <a href="service-inner-2.php" class="single-exp-wrap">
                    <b></b>
                    <img src="https://images.pexels.com/photos/7841856/pexels-photo-7841856.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" alt="">
                    <div class="exp-cnt">
                        <span>
                            <img src="img/exp-icon-2.png" alt="">
                        </span>
                        <label>Arbitration</label>
                        <p>We approach debt recovery issues without threatening clients’ existing business relations, time and money...</p>
                        <strong class="read-more-btn">View More <i class="ms-2 bi bi-arrow-right"></i></strong>
                    </div>
                </a>
            </div>

            <div class="col-12 col-md-6 col-lg-4">
                <a href="service-inner-3.php" class="single-exp-wrap">
                    <b></b>
                    <img src="https://images.pexels.com/photos/7876050/pexels-photo-7876050.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" alt="">
                    <div class="exp-cnt">
                        <span>
                            <img src="img/exp-icon-2.png" alt="">
                        </span>
                        <label>Debt Recovery</label>
                        <p>We approach debt recovery issues without threatening clients’ existing business relations, time and money...</p>
                        <strong class="read-more-btn">View More <i class="ms-2 bi bi-arrow-right"></i></strong>
                    </div>
                </a>
            </div>

            <div class="col-12 col-md-6 col-lg-4">
                <a href="service-inner-10.php" class="single-exp-wrap">
                    <b></b>
                    <img src="https://images.pexels.com/photos/48148/document-agreement-documents-sign-48148.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" alt="">
                    <div class="exp-cnt">
                        <span>
                            <img src="img/exp-icon-4.png" alt="">
                        </span>
                        <label>Legal Consultation</label>
                        <p>We approach debt recovery issues without threatening clients’ existing business relations, time and money...</p>
                        <strong class="read-more-btn">View More <i class="ms-2 bi bi-arrow-right"></i></strong>
                    </div>
                </a>
            </div>

            <div class="col-12 col-md-6 col-lg-4">
                <a href="service-inner-4.php" class="single-exp-wrap">
                    <b></b>
                    <img src="https://images.pexels.com/photos/8730980/pexels-photo-8730980.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" alt="">
                    <div class="exp-cnt">
                        <span>
                            <img src="img/exp-icon-5.png" alt="">
                        </span>
                        <label>Company Setup</label>
                        <p>We approach debt recovery issues without threatening clients’ existing business relations, time and money...</p>
                        <strong class="read-more-btn">View More <i class="ms-2 bi bi-arrow-right"></i></strong>
                    </div>
                </a>
            </div>

            <div class="col-12 col-md-6 col-lg-4">
                <a href="service-inner-5.php" class="single-exp-wrap">
                    <b></b>
                    <img src="https://images.unsplash.com/photo-1505664194779-8beaceb93744?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80" alt="">
                    <div class="exp-cnt">
                        <span>
                            <img src="img/exp-icon-6.png" alt="">
                        </span>
                        <label>Corporate Services</label>
                        <p>We approach debt recovery issues without threatening clients’ existing business relations, time and money...</p>
                        <strong class="read-more-btn">View More <i class="ms-2 bi bi-arrow-right"></i></strong>
                    </div>
                </a>
            </div>

            <div class="col-12 col-md-6 col-lg-4">
                <a href="service-inner-6.php" class="single-exp-wrap">
                    <b></b>
                    <img src="https://images.pexels.com/photos/7841856/pexels-photo-7841856.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" alt="">
                    <div class="exp-cnt">
                        <span>
                            <img src="img/exp-icon-7.png" alt="">
                        </span>
                        <label>Company Representation</label>
                        <p>We approach debt recovery issues without threatening clients’ existing business relations, time and money...</p>
                        <strong class="read-more-btn">View More <i class="ms-2 bi bi-arrow-right"></i></strong>
                    </div>
                </a>
            </div>

            <div class="col-12 col-md-6 col-lg-4">
                <a href="service-inner-7.php" class="single-exp-wrap">
                    <b></b>
                    <img src="https://images.pexels.com/photos/7876050/pexels-photo-7876050.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" alt="">
                    <div class="exp-cnt">
                        <span>
                            <img src="img/exp-icon-8.png" alt="">
                        </span>
                        <label>PRO Services</label>
                        <p>We approach debt recovery issues without threatening clients’ existing business relations, time and money...</p>
                        <strong class="read-more-btn">View More <i class="ms-2 bi bi-arrow-right"></i></strong>
                    </div>
                </a>
            </div>

            <div class="col-12 col-md-6 col-lg-4">
                <a href="service-inner-8.php" class="single-exp-wrap">
                    <b></b>
                    <img src="https://images.pexels.com/photos/48148/document-agreement-documents-sign-48148.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" alt="">
                    <div class="exp-cnt">
                        <span>
                            <img src="img/exp-icon-9.png" alt="">
                        </span>
                        <label>Third-Party Audit</label>
                        <p>We approach debt recovery issues without threatening clients’ existing business relations, time and money...</p>
                        <strong class="read-more-btn">View More <i class="ms-2 bi bi-arrow-right"></i></strong>
                    </div>
                </a>
            </div>

            <div class="col-12 col-md-6 col-lg-4">
                <a href="service-inner-9.php" class="single-exp-wrap">
                    <b></b>
                    <img src="https://images.pexels.com/photos/8730980/pexels-photo-8730980.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" alt="">
                    <div class="exp-cnt">
                        <span>
                            <img src="img/exp-icon-10.png" alt="">
                        </span>
                        <label>Trademark Registration</label>
                        <p>We approach debt recovery issues without threatening clients’ existing business relations, time and money...</p>
                        <strong class="read-more-btn">View More <i class="ms-2 bi bi-arrow-right"></i></strong>
                    </div>
                </a>
            </div>


            <div class="col-12 text-center my-3 my-md-4 d-flex justify-content-center">
                <a href="#" class="read-more-btn d-flex align-items-center">Load More <i class="ms-2 bi bi-arrow-right"></i></a>
            </div>

        </div>
    </div>
</section>

<section class="container-fluid contact_ad ad_1">
    <div class="row align-items-center">
        <div class="col-12 p-0">
            <div class="">
                <h2>are you looking for a consultation ?</h1>
                <p>Our expert team of professionals adhere to the legal needs of clients with our wide range of high-quality services.</p>
                <div class="d-flex justify-content-center">
                    <a href="#" class="fill-btn"><span>Practice Area</span></a>
                    <a href="#" class="outline-btn"><span>Contact</span></a>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>
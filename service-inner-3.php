<?php include 'header.php';?>

<!------------------------------------------------------------------edititing start here-------------------------------------------------->


<section class="container-fluid inner-banner">
    <div class="row align-items-center text-center">
        <div class="inner-banner-img-wrap">
            <img src="https://images.unsplash.com/photo-1473186505569-9c61870c11f9?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80" alt="">
        </div>
        <h2>Debt Recovery</h2>
    </div>
</section>

<section class="container-fluid serv-inner-sec">
    <div class="container">
        <div class="row">

            <div class="col-12 col-md-6 col-lg-8">
                <div class="serv-img-wrap">
                    <img src= "https://images.unsplash.com/photo-1562564055-71e051d33c19?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80" alt="">
                </div>
                
                <div class="mb-4">
                    <h3>We are here to resolve</h3>
                    <p>Our dedicated team of professionals have the crucial experience in debt recovery services around the globe. Do you witness complex financial situations?
                    Get your consultation done with us before the increasing amount of debt can cause disaster. 
                    With our decade of experience in all judicial sectors, we always ensure that legal ethics are maintained throughout our journey.</p>
                </div>
                
                <div class="mb-4">
                    <h3>Let us harness all our experience for your justice </h3>
                    <p>We approach debt recovery issues without threatening clients’ existing business relations, time and money. 
                    Let us work closely with our clients and monitor dispute resolutions as well. We hold keen priority in caring for clients’ interests. 
                    Let us negotiate the problems, clarify issues, make options for settlements, and ultimately maintain client relations intact.
                    We support a cost-effective solution in debt recovery cases. We are famed as professional negotiators and assist in drafting settlement agreements, 
                    initiating legal proceedings, and guiding in finalizing instalment payments..</p>
                </div>
                </div>
                
            <!------------------------------------------------------------------edititing end here-------------------------------------------------->

         <div class="col-12 col-md-6 col-lg-4">
                <div class="practice-area">
                    <h4>Practice Area</h4>
                    <div class="">
                        <a href="#" class="active">ligitation <i class="bi bi-chevron-right"></i></a>
                        <a href="#">Arbitration <i class="bi bi-chevron-right"></i></a>
                        <a href="#">Debt Recovery <i class="bi bi-chevron-right"></i></a>
                        <a href="#">Legal Consultation<i class="bi bi-chevron-right"></i></a>
                        <a href="#">Company Setup <i class="bi bi-chevron-right"></i></a>
                        <a href="#">Corporate Services <i class="bi bi-chevron-right"></i></a>
                        <a href="#">Company Representation <i class="bi bi-chevron-right"></i></a>
                        <a href="#">PRO Services<i class="bi bi-chevron-right"></i></a>
                        <a href="#">Third-Party Audit <i class="bi bi-chevron-right"></i></a>
                        <a href="#">Trademark Registration <i class="bi bi-chevron-right"></i></a>
                    </div>

                </div>

                <div class="practice-area brochure">
                    <h4>our brochure</h4>
                    <div class="bro-div">
                        <p>At GLC, we offer affordable smart legal solutions serving local as well as overseas clients. We understand your concern and use our professionalism</p>
                    </div>
                    <a href="#" class="read-more-btn"><i class="bi bi-file-earmark-text me-2"></i>Download Brochure</a>
                </div>


            </div>

        </div>
    </div>
</section>

<section class="container-fluid contact_ad ad_1">
    <div class="row align-items-center">
        <div class="col-12 p-0">
            <div class="">
                <h2>are you looking for a consultation ?</h1>
                <p>Our expert team of professionals adhere to the legal needs of clients with our wide range of high-quality services.</p>
                <div class="d-flex justify-content-center">
                    <a href="#" class="fill-btn"><span>Practice Area</span></a>
                    <a href="#" class="outline-btn"><span>Contact</span></a>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>
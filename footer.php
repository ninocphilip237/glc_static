<section class="container-fluid contact-form-wrap">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12 col-md-6">
                        <div class="dtl-wrap">
                            <span>
                                <img src="img/contact-1.svg" alt="">
                            </span>
                            <div class="main-champ">
                                <label>Main Chamber</label>
                                <a href="#"><p>Khalifa Bin Zayed The First Street United Arab Bank Building Office - 072, Abu Dhabi, UAE</p></a>
                            </div>
                        </div>

                        <div class="dtl-wrap">
                            <span>
                                <img src="img/contact-2.svg" alt="">
                            </span>
                            <div>
                                <label>Mail address</label>
                                <a href="mailto:info@glclawyers.com"><p>info@glclawyers.com</p></a>
                            </div>
                        </div>

                        <div class="dtl-wrap">
                            <span>
                                <img src="img/contact-3.svg" alt="">
                            </span>
                            <div>
                                <label>contact number</label>
                                <a href="tel:7214-965-147"><p>7214 965 147</p></a>
                                <a href="tel:7214-236-987"><p>7214 236 987</p></a>
                            </div>
                        </div>

                        <div class="dtl-wrap">
                            <span>
                                <img src="img/contact-4.svg" alt="">
                            </span>
                            <div>
                                <label>working time</label>
                                <p>Sun-Thu :9am - 7pm</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-6">
                        <form class="row">
                            <div class="col-12 my-2 my-md-4">
                                <label class="mb-2"><span>Contact Us</span></label>
                                <h2>get In Touch</h2>
                            </div>
                            <div class="col-12">
                                <input type="text" placeholder="Name">
                            </div>

                            <div class="col-12">
                                <input type="text" placeholder="Email">
                            </div>

                            <div class="col-12">
                                <input type="text" placeholder="Phone">
                            </div>

                            <div class="col-12">
                                <textarea type="text" placeholder="Your Message"></textarea>
                            </div>

                            <div class="col-12">
                                <a href="#" class="fill-btn"><span>Submit</span></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>


        <section class="container-fluid">
            <div class="row">
                <iframe class="p-0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3630.8327935268867!2d54.35746951537176!3d24.49124966598629!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5e66649a9f7e7b%3A0x2f6a4a33deafe9fb!2sUnited%20Arab%20Bank!5e0!3m2!1sen!2sin!4v1668776277037!5m2!1sen!2sin" height="450" style="border:0;" allowfullscreen="" ></iframe>
            </div>
        </section>

        

        <footer class="container-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">Services</a></li>
                            <li><a href="#">Contact us</a></li>
                            <li><a href="#">Terms and conditions</a></li>
                            <li><a href="#">privacy policy</a></li>
                        </ul>
                    </div>

                    <div class="col-12">
                        <div class="reach-us">
                            <a href="#">
                                <i class="bi bi-mailbox"></i>
                                <span>info@glclawyers.com</span>
                            </a>
    
                            <a href="#">
                                <i class="bi bi-telephone"></i>
                                <span>7214 965 147</span>
                            </a>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="social_wrap">
                            <a class="facebook" href="#"></a>
                            <a class="twitter" href="#"></a>
                            <a class="youtube" href="#"></a>
                            <a class="instagram" href="#"></a>
                        </div>
                    </div>

                </div>
            </div>
            <div class="copyright">© GLCleagelconsultation 2022. all rights reserved.</div>
        </footer>



        <script src="js/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
    
    </body>
</html>
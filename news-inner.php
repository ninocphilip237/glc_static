<?php include 'header.php';?>

<section class="container-fluid inner-banner">
    <div class="row align-items-center text-center">
        <div class="inner-banner-img-wrap">
            <img src="https://images.unsplash.com/photo-1511578314322-379afb476865?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1169&q=80" alt="">
        </div>
        <h2>Nullam feugiat et quam in accumsan.</h2>
    </div>
</section>

<section class="container-fluid serv-inner-sec">
    <div class="container">
        <div class="row"> 

            <div class="col-12 col-md-6 col-lg-8">
                <div class="serv-img-wrap brdr">
                    <img src="https://images.unsplash.com/photo-1502465771179-51f3535da42c?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1169&q=80" alt="">
                </div>
                <div class="mb-4">
                    <div class="admin-dtl">
                        <p><span>By Admin</span> | <span>March 12, 2021</span></p>
                    </div>
                    <h3>Nullam feugiat et quam in accumsan.</h3>
                    <p>Integer quis libero semper, interdum odio non, consequat sem. Quisque woner pretium, quamtenti
                        utendi lacinianti ultricies, est urna cursus purus, ut tristique purusenali pretium, quam ut
                        laciniaun est urna cursusvoli purus, ut tristique purusenali mauris.....
                        <br>Vestibulum eget mauris dui. Proin luctus est lacus, eu lobortis orci dignissim at. Ut nec
                         vulputateri nisl. Mauris vel dolor augue. Praesent sit amet eros dui. Sed porta gravida mollis.
                         In odio dolorcoul maximus a finibus eget, euismod vitae tellus. Vivamus condimentum nulla rhoncus
                          ipsum jougara vehicula, quis bibendum massa auctor. Suspendisse potenti. Nam vulputate diam tempus
                           orcilagri rhoncus tincidunt et nec dolor. Curabitur id nunc hendrerit</p>
                </div>

                <div class="quote-right">
                    <p>Integer quis libero semper, interdum odio non, consequat sem. Qui woner pretium, tristique purusenali pretium, quam ut laciniaun est urna.quamtenti utendi lacinianti ultricies, est urna cursus purus, ut tristique purusenali pretium</p>
                </div>

                <div class="serv-img-wrap wid-hig">
                    <img src="https://images.unsplash.com/photo-1542744173-8e7e53415bb0?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80" alt="">
                </div>
                <div class="mb-4">
                    <h3>Nullam feugiat et quam in accumsan.</h3>
                    <p>Vestibulum eget mauris dui. Proin luctus est lacus, eu lobortis orci dignissim at. Ut nec vulputateri nisl.
                        Mauris vel dolor augue. Praesent sit amet eros dui. Sed porta gravida mollis. In odio dolorcoul maximus a
                        finibus eget, euismod vitae tellus. Vivamus condimentum nulla rhoncus ipsum jougara vehicula, quis bibendum
                         massa auctor. Suspendisse potenti. Nam vulputate diam tempus orcilagri rhoncus tincidunt et nec dolor.
                         Curabitur id nunc hendrerit</p>
                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-4">

                <div class="practice-area">
                    <h4>category</h4>
                    <div class="">
                        <a href="#">category-1 <i>(3)</i></a>
                        <a href="#">category-2 <i>(5)</i></a>
                        <a href="#">category-3 <i>(2)</i></a>
                        <a href="#">category-4<i>(1)</i></a>
                    </div>
                </div>

                <div class="practice-area recent">
                    <h4>recent posts</h4>
                    <div class="">
                        <a href="#">
                            <span>
                                <img src="https://images.unsplash.com/photo-1589391886645-d51941baf7fb?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80" alt="">
                            </span>
                            <div>
                                <i>March 12, 2021</i>
                                <p>What qualities make a great air hostess?</p>
                            </div>
                        </a>
                        <a href="#">
                            <span>
                                <img src="https://images.unsplash.com/photo-1589391886645-d51941baf7fb?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80" alt="">
                            </span>
                            <div>
                                <i>March 12, 2021</i>
                                <p>What qualities make a great air hostess?</p>
                            </div>
                        </a>
                        <a href="#">
                            <span>
                                <img src="https://images.unsplash.com/photo-1589391886645-d51941baf7fb?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80" alt="">
                            </span>
                            <div>
                                <i>March 12, 2021</i>
                                <p>What qualities make a great air hostess?</p>
                            </div>
                        </a>
                        <a href="#">
                            <span>
                                <img src="https://images.unsplash.com/photo-1589391886645-d51941baf7fb?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80" alt="">
                            </span>
                            <div>
                                <i>March 12, 2021</i>
                                <p>What qualities make a great air hostess?</p>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="practice-area tags">
                    <h4>Tags</h4>
                    <div class="">
                        <a href="#">Law</a>
                        <a href="#">Cases</a>
                        <a href="#">Human Rights</a>
                        <a href="#">Person</a>
                    </div>
                </div>

            </div>

        </div>
    </div>
</section>

<section class="container-fluid contact_ad ad_1">
    <div class="row align-items-center">
        <div class="col-12 p-0">
            <div class="">
                <h2>are you looking for a consultation ?</h1>
                <p>Our expert team of professionals adhere to the legal needs of clients with our wide range of high-quality services.</p>
                <div class="d-flex justify-content-center">
                    <a href="#" class="fill-btn"><span>Practice Area</span></a>
                    <a href="#" class="outline-btn"><span>Contact</span></a>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>

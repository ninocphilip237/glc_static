<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Welcome to GLC Lawyers</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Playfair+Display:ital,wght@0,400;0,500;0,600;0,700;0,800;0,900;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.2/font/bootstrap-icons.css">
    </head>
    <body>

        <header class="container-fluid">
            <div class="container">
                <div class="row align-items-center">
                    <nav class="navbar navbar-expand-lg">
                        <a class="navbar-brand order-1" href="index.php">
                            <img src="img/glc-logo.svg" alt="">
                        </a>
                        <button class="navbar-toggler order-2 order-md-4 order-lg-2" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse order-3" id="navbarNavAltMarkup">
                            <div class="navbar-nav">
                                <a class="nav-link active" aria-current="page" href="index.php">Home </a>
                                <a class="nav-link" href="about.php">About Us</a>
                                <a class="nav-link" href="service-listing.php">Services</a>
                                <a class="nav-link" href="news-event-listing.php">News & Events</a>
                                <a class="nav-link" href="contact.php">Contact us</a>
                                <a href="contact.php" class="consultation order-3 d-inline-block d-md-none">Request consultation<i class="bi bi-arrow-right"></i></a>
                            </div>
                        </div>
                        <a href="contact.php" class="consultation order-4 order-md-2 order-lg-4 d-none d-md-inline-block">Request consultation<i class="bi bi-arrow-right"></i></a>
                    </nav>
                </div>
            </div>
        </header>
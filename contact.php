<?php include 'header.php';?>

<section class="container-fluid inner-banner">
    <div class="row align-items-center text-center">
        <div class="inner-banner-img-wrap">
            <img src="https://images.unsplash.com/photo-1589829545856-d10d557cf95f?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80" alt="">
        </div>
        <h2>Contact Us</h2>
    </div>
</section>

<?php include 'footer.php';?>
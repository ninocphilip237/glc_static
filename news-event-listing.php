<?php include 'header.php';?>


<section class="container-fluid inner-banner">
    <div class="row align-items-center text-center">
        <div class="inner-banner-img-wrap">
            <img src="https://images.unsplash.com/photo-1511578314322-379afb476865?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1169&q=80" alt="">
        </div>
        <h2>News & Events</h2>
    </div>
</section>

<section class="container-fluid my-5">
    <div class="container">
        <div class="row">

            <div class="col-12 col-md-4">
                <div class="news-wrap">
                    <span>
                        <img src="https://images.unsplash.com/photo-1505488387362-48bc38155987?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1171&q=80" alt="">
                    </span>
                    <h3>we can achieve anything by heart</h3>
                    <div class="date-wrap">
                        <label>November 30, 2022</label>
                        <a href="news-inner.php" class="read-more-btn">Read More <i class="ms-2 bi bi-arrow-right"></i></a>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-4">
                <div class="news-wrap">
                    <span>
                        <img src="https://images.unsplash.com/photo-1589391886645-d51941baf7fb?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80" alt="">
                    </span>
                    <h3>we can achieve anything by heart</h3>
                    <div class="date-wrap">
                        <label>November 30, 2022</label>
                        <a href="news-inner.php" class="read-more-btn">Read More <i class="ms-2 bi bi-arrow-right"></i></a>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-4">
                <div class="news-wrap">
                    <span>
                        <img src="https://images.unsplash.com/photo-1589216532372-1c2a367900d9?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1171&q=80" alt="">
                    </span>
                    <h3>we can achieve anything by heart</h3>
                    <div class="date-wrap">
                        <label>November 30, 2022</label>
                        <a href="news-inner.php" class="read-more-btn">Read More <i class="ms-2 bi bi-arrow-right"></i></a>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-4">
                <div class="news-wrap">
                    <span>
                        <img src="https://images.unsplash.com/photo-1589216532372-1c2a367900d9?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1171&q=80" alt="">
                    </span>
                    <h3>we can achieve anything by heart</h3>
                    <div class="date-wrap">
                        <label>November 30, 2022</label>
                        <a href="news-inner.php" class="read-more-btn">Read More <i class="ms-2 bi bi-arrow-right"></i></a>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-4">
                <div class="news-wrap">
                    <span>
                        <img src="https://images.unsplash.com/photo-1589216532372-1c2a367900d9?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1171&q=80" alt="">
                    </span>
                    <h3>we can achieve anything by heart</h3>
                    <div class="date-wrap">
                        <label>November 30, 2022</label>
                        <a href="news-inner.php" class="read-more-btn">Read More <i class="ms-2 bi bi-arrow-right"></i></a>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-4">
                <div class="news-wrap">
                    <span>
                        <img src="https://images.unsplash.com/photo-1589216532372-1c2a367900d9?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1171&q=80" alt="">
                    </span>
                    <h3>we can achieve anything by heart</h3>
                    <div class="date-wrap">
                        <label>November 30, 2022</label>
                        <a href="news-inner.php" class="read-more-btn">Read More <i class="ms-2 bi bi-arrow-right"></i></a>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-4">
                <div class="news-wrap">
                    <span>
                        <img src="https://images.unsplash.com/photo-1589216532372-1c2a367900d9?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1171&q=80" alt="">
                    </span>
                    <h3>we can achieve anything by heart</h3>
                    <div class="date-wrap">
                        <label>November 30, 2022</label>
                        <a href="news-inner.php" class="read-more-btn">Read More <i class="ms-2 bi bi-arrow-right"></i></a>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-4">
                <div class="news-wrap">
                    <span>
                        <img src="https://images.unsplash.com/photo-1589216532372-1c2a367900d9?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1171&q=80" alt="">
                    </span>
                    <h3>we can achieve anything by heart</h3>
                    <div class="date-wrap">
                        <label>November 30, 2022</label>
                        <a href="news-inner.php" class="read-more-btn">Read More <i class="ms-2 bi bi-arrow-right"></i></a>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-4">
                <div class="news-wrap">
                    <span>
                        <img src="https://images.unsplash.com/photo-1589216532372-1c2a367900d9?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1171&q=80" alt="">
                    </span>
                    <h3>we can achieve anything by heart</h3>
                    <div class="date-wrap">
                        <label>November 30, 2022</label>
                        <a href="news-inner.php" class="read-more-btn">Read More <i class="ms-2 bi bi-arrow-right"></i></a>
                    </div>
                </div>
            </div>

            <div class="col-12 text-center my-3 my-md-5 d-flex justify-content-center">
                <a href="#" class="read-more-btn d-flex align-items-center">Load More <i class="ms-2 bi bi-arrow-right"></i></a>
            </div>

        </div>
    </div>
</section>

<section class="container-fluid contact_ad ad_1">
    <div class="row align-items-center">
        <div class="col-12 p-0">
            <div class="">
                <h2>are you looking for a consultation ?</h1>
                <p>Our expert team of professionals adhere to the legal needs of clients with our wide range of high-quality services.</p>
                <div class="d-flex justify-content-center">
                    <a href="#" class="fill-btn"><span>Practice Area</span></a>
                    <a href="#" class="outline-btn"><span>Contact</span></a>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>
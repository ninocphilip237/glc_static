<?php include 'header.php';?>

<section class="container-fluid inner-banner">
    <div class="row align-items-center text-center">
        <div class="inner-banner-img-wrap">
            <img src="https://images.unsplash.com/photo-1488190211105-8b0e65b80b4e?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80" alt="">
        </div>
        <h2>About Us</h2>
    </div>
</section>

<section class="container-fluid py-3 py-md-5">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 col-md-5">
                <label>about <span>GLC</span></label>
                <h2>Client matters to us!!</h2>
                <p>With over _______ years of expertise, GLC is an award-winning legal consulting firm with a global presence
                     serving local and international clients. We are famed as one of the pioneers of Legal Consulting firms
                      in Abu Dhabi with proven track records in legal cases.</p>
                <a href="#" class="read-more-btn"><i class="bi bi-file-earmark-text me-2"></i>Download Brochure</a>
            </div>
            <div class="col-12 col-md-7">
                <div class="abt-img-wrap">
                    <img src="img/abt-image.png" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="container-fluid overview-sec">
    <div class="row align-items-center">
        <div class="col-12 col-md-6 p-0">
            <div class="over-sec-wrap">
                <img src="https://images.unsplash.com/photo-1589391886645-d51941baf7fb?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80" alt="">
            </div>
        </div>
        <div class="col-12 col-md-6">
            <div class="over-cnt-div">
                <label>Our <span>overview</span></label>
                <h2>Your Satisfaction, Our Success!!</h2>
                <p>We stand close to our customers. Our innovative approach allowed us to identify & manage clients’ risks,
                 and provide timely legal advice at the right times. We are specialized in the areas of litigation,
                  arbitration, debt recovery, legal consultation, company setup, company representation,
                   corporate services, PRO services, third-party audit, and trademark registration.</p>
            </div>
        </div>
    </div>
</section>

<section class="container-fluid miss-vis-sec">
    <div class="container">
        <div class="row justify-content-evenly">

            <div class="col-12 text-center my-2 my-md-5">
                <label>Our <span>Mission and Vision</span></label>
                <h2>What Make Us Special</h2>
            </div>

            <div class="col-12 col-md-6 col-lg-5">
                <div class="full-sec text-center">
                    <span>
                        <img src="img/miss-icon.svg" alt="">
                    </span>
                    <h2>Mission</h2>
                    <p>As a team, we always strive to approach a research-oriented solution tailored to meet individual 
                        legal needs and deliver the highest quality professional legal services at affordable costs.</p>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-5">
                <div class="full-sec text-center">
                    <span>
                        <img src="img/vis-icon.svg" alt="">
                    </span>
                    <h2>Vision</h2>
                    <p>To become industry leaders in legal services with a global presence and to deliver advanced professional legal solutions to those who approach us.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="container-fluid">
    <div class="container">
        <div class="row align-items-center">

            <div class="col-12 text-center my-2 my-md-5">
                <label>Meet <span>Our Team</span></label>
                <h2>Meet Our Attorneys</h2>
                <p class="text-center">A company is nothing without its team. Our legal team with extensive experience is expertise in dealing with cross-border and domestic disputes. We are confident in achieving the best results for our clients.</p>
            </div>

            <div class="col-12 col-md-6 col-lg-4">
                <div class="atterny-img">
                    <img src="https://images.unsplash.com/photo-1558222218-b7b54eede3f3?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80" alt="">
                </div>
                <div class="attorny-cnt">
                    <h3>Kelvine Pitterson</h3>
                    <span>Lawyer</span>
                    <i class="bi bi-hand-thumbs-up"></i>
                    <p>3250 Cases</p>
                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-4">
                <div class="atterny-img">
                    <img src="https://images.unsplash.com/photo-1558222218-b7b54eede3f3?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80" alt="">
                </div>
                <div class="attorny-cnt">
                    <h3>Kelvine Pitterson</h3>
                    <span>Lawyer</span>
                    <i class="bi bi-hand-thumbs-up"></i>
                    <p>3250 Cases</p>
                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-4">
                <div class="atterny-img">
                    <img src="https://images.unsplash.com/photo-1558222218-b7b54eede3f3?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80" alt="">
                </div>
                <div class="attorny-cnt">
                    <h3>Kelvine Pitterson</h3>
                    <span>Lawyer</span>
                    <i class="bi bi-hand-thumbs-up"></i>
                    <p>3250 Cases</p>
                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-4">
                <div class="atterny-img">
                    <img src="https://images.unsplash.com/photo-1558222218-b7b54eede3f3?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80" alt="">
                </div>
                <div class="attorny-cnt">
                    <h3>Kelvine Pitterson</h3>
                    <span>Lawyer</span>
                    <i class="bi bi-hand-thumbs-up"></i>
                    <p>3250 Cases</p>
                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-4">
                <div class="atterny-img">
                    <img src="https://images.unsplash.com/photo-1558222218-b7b54eede3f3?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80" alt="">
                </div>
                <div class="attorny-cnt">
                    <h3>Kelvine Pitterson</h3>
                    <span>Lawyer</span>
                    <i class="bi bi-hand-thumbs-up"></i>
                    <p>3250 Cases</p>
                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-4">
                <div class="atterny-img">
                    <img src="https://images.unsplash.com/photo-1558222218-b7b54eede3f3?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80" alt="">
                </div>
                <div class="attorny-cnt">
                    <h3>Kelvine Pitterson</h3>
                    <span>Lawyer</span>
                    <i class="bi bi-hand-thumbs-up"></i>
                    <p>3250 Cases</p>
                </div>
            </div>

        </div>
    </div>
</section>

<section class="container-fluid why-us our-attorney-wrap">
    <div class="container">
        <div class="row">

            <div class="col-12 text-center my-2 my-md-5">
                <h2>Why GLC ?</h2>
            </div>

            <div class="col-12 col-md-6 col-lg-4">
                <div class="why-us-wrap">
                    <span>
                        <img src="img/satisfy.svg" alt="">
                    </span>
                    <h4>Exceed customer satisfaction</h4>
                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-4">
                <div class="why-us-wrap">
                    <span>
                        <img src="img/satisfy.svg" alt="">
                    </span>
                    <h4>Highest standard of approach to legal solutions</h4>
                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-4">
                <div class="why-us-wrap">
                    <span>
                        <img src="img/satisfy.svg" alt="">
                    </span>
                    <h4>Professional excellence & quality legal advice</h4>
                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-4">
                <div class="why-us-wrap">
                    <span>
                        <img src="img/satisfy.svg" alt="">
                    </span>
                    <h4>Passionate in pursuing client’s legal needs and protecting their rights</h4>
                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-4">
                <div class="why-us-wrap">
                    <span>
                        <img src="img/satisfy.svg" alt="">
                    </span>
                    <h4>International trained legal professionals</h4>
                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-4">
                <div class="why-us-wrap">
                    <span>
                        <img src="img/satisfy.svg" alt="">
                    </span>
                    <h4>Passionate in pursuing client’s legal needs and protecting their rights</h4>
                </div>
            </div>

        </div>
    </div>
</section>

<?php include 'footer.php';?>
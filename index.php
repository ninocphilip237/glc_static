<?php include 'header.php';?>

        <section class="container-fluid banner-wrap">
            <div class="row">
                <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="false">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="https://images.unsplash.com/photo-1505663912202-ac22d4cb3707?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80" class="d-block w-100" alt="...">
                            <div class="carousel-caption cnt-sec">
                                <i><span>GLC</span> LAW FIRM</i>
                                <h1>Excellence in law</h1>
                                <p>Our services are duly surrounded and delivered by expert legal professional</p>
                                <div class="d-flex justify-content-center">
                                    <a href="#" class="fill-btn"><span>Practice Area</span></a>
                                    <a href="#" class="outline-btn"><span>Contact</span></a>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="https://images.unsplash.com/photo-1423592707957-3b212afa6733?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1632&q=80" class="d-block w-100" alt="...">
                            <div class="carousel-caption cnt-sec">
                                <i><span>GLC</span> LAW FIRM</i>
                                <h1>Excellence in law</h1>
                                <p>Our services are duly surrounded and delivered by expert legal professional</p>
                                <div class="d-flex justify-content-center">
                                    <a href="#" class="fill-btn"><span>Practice Area</span></a>
                                    <a href="#" class="outline-btn"><span>Contact</span></a>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="https://images.unsplash.com/photo-1450101499163-c8848c66ca85?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80" class="d-block w-100" alt="...">
                            <div class="carousel-caption cnt-sec">
                                <i><span>GLC</span> LAW FIRM</i>
                                <h1>Excellence in law</h1>
                                <p>Our services are duly surrounded and delivered by expert legal professional</p>
                                <div class="d-flex justify-content-center">
                                    <a href="#" class="fill-btn"><span>Practice Area</span></a>
                                    <a href="#" class="outline-btn"><span>Contact</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                        <span class="bi bi-arrow-left-circle" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                        <span class="bi bi-arrow-right-circle" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                </div>
            </div>
        </section>

        <section class="container-fluid abt-sec">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12 col-md-4">
                        <div class="welocome-div">
                            <h2>Welcome To</h2>
                            <span>GLC</span>
                            <i>Law firm</i>
                        </div>
                    </div>

                    <div class="col-12 col-md-8 px-md-5">
                        <p>At GLC, we offer affordable smart legal solutions serving local as well as overseas clients. 
                            We understand your concern and use our professionalism to resolve a wide range of legal problems. 
                            Our vibrant team uses the latest technology to offer result-oriented solutions nationwide...
                            <a href="#" class="read-more-btn">Read More</a>
                        </p>
                        <a href="#" class="read-more-btn"><i class="bi bi-file-earmark-text me-2"></i>Download Brochure</a>
                    </div>
                </div>
            </div>
        </section>

        <section class="container-fluid area-of-exp">
            <div class="row align-items-center">
                <div class="col-12 text-center my-2 my-md-5">
                    <label>Area of <span>expertise</span></label>
                    <h2>Incredible services</h2>
                </div>

                <div class="area-exp-full-wrap">

                    <a href="service-inner-1.php" class="single-exp-wrap">
                        <b></b>
                        <img src="https://images.unsplash.com/photo-1505664194779-8beaceb93744?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80" alt="">
                        <div class="exp-cnt">
                            <span>
                                <img src="img/exp-icon-1.png" alt="">
                            </span>
                            <label>Litigation</label>
                            <p>We approach debt recovery issues without threatening clients’ existing business relations, time and money...</p>
                            <strong class="read-more-btn">View More <i class="ms-2 bi bi-arrow-right"></i></strong>
                        </div>
                    </a>

                    <a href="service-inner-2.php" class="single-exp-wrap">
                        <b></b>
                        <img src="https://images.pexels.com/photos/7841856/pexels-photo-7841856.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" alt="">
                        <div class="exp-cnt">
                            <span>
                                <img src="img/exp-icon-2.png" alt="">
                            </span>
                            <label>Arbitration</label>
                            <p>We approach debt recovery issues without threatening clients’ existing business relations, time and money...</p>
                            <strong class="read-more-btn">View More <i class="ms-2 bi bi-arrow-right"></i></strong>
                        </div>
                    </a>

                    <a href="service-inner-3.php" class="single-exp-wrap">
                        <b></b>
                        <img src="https://images.pexels.com/photos/7876050/pexels-photo-7876050.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" alt="">
                        <div class="exp-cnt">
                            <span>
                                <img src="img/exp-icon-2.png" alt="">
                            </span>
                            <label>Debt Recovery</label>
                            <p>We approach debt recovery issues without threatening clients’ existing business relations, time and money...</p>
                            <strong class="read-more-btn">View More <i class="ms-2 bi bi-arrow-right"></i></strong>
                        </div>
                    </a>

                    <a href="service-inner-10.php" class="single-exp-wrap">
                        <b></b>
                        <img src="https://images.pexels.com/photos/48148/document-agreement-documents-sign-48148.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" alt="">
                        <div class="exp-cnt">
                            <span>
                                <img src="img/exp-icon-4.png" alt="">
                            </span>
                            <label>Legal Consultation</label>
                            <p>We approach debt recovery issues without threatening clients’ existing business relations, time and money...</p>
                            <strong class="read-more-btn">View More <i class="ms-2 bi bi-arrow-right"></i></strong>
                        </div>
                    </a>

                    <a href="service-inner-4.php" class="single-exp-wrap">
                        <b></b>
                        <img src="https://images.pexels.com/photos/8730980/pexels-photo-8730980.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" alt="">
                        <div class="exp-cnt">
                            <span>
                                <img src="img/exp-icon-5.png" alt="">
                            </span>
                            <label>Company Setup</label>
                            <p>We approach debt recovery issues without threatening clients’ existing business relations, time and money...</p>
                            <strong class="read-more-btn">View More <i class="ms-2 bi bi-arrow-right"></i></strong>
                        </div>
                    </a>

                    <a href="service-inner-5.php" class="single-exp-wrap">
                        <b></b>
                        <img src="https://images.unsplash.com/photo-1505664194779-8beaceb93744?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80" alt="">
                        <div class="exp-cnt">
                            <span>
                                <img src="img/exp-icon-6.png" alt="">
                            </span>
                            <label>Corporate Services</label>
                            <p>We approach debt recovery issues without threatening clients’ existing business relations, time and money...</p>
                            <strong class="read-more-btn">View More <i class="ms-2 bi bi-arrow-right"></i></strong>
                        </div>
                    </a>

                    <a href="service-inner-6.php" class="single-exp-wrap">
                        <b></b>
                        <img src="https://images.pexels.com/photos/7841856/pexels-photo-7841856.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" alt="">
                        <div class="exp-cnt">
                            <span>
                                <img src="img/exp-icon-7.png" alt="">
                            </span>
                            <label>Company Representation</label>
                            <p>We approach debt recovery issues without threatening clients’ existing business relations, time and money...</p>
                            <strong class="read-more-btn">View More <i class="ms-2 bi bi-arrow-right"></i></strong>
                        </div>
                    </a>

                    <a href="service-inner-7.php" class="single-exp-wrap">
                        <b></b>
                        <img src="https://images.pexels.com/photos/7876050/pexels-photo-7876050.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" alt="">
                        <div class="exp-cnt">
                            <span>
                                <img src="img/exp-icon-8.png" alt="">
                            </span>
                            <label>PRO Services</label>
                            <p>We approach debt recovery issues without threatening clients’ existing business relations, time and money...</p>
                            <strong class="read-more-btn">View More <i class="ms-2 bi bi-arrow-right"></i></strong>
                        </div>
                    </a>

                    <a href="service-inner-8.php" class="single-exp-wrap">
                        <b></b>
                        <img src="https://images.pexels.com/photos/48148/document-agreement-documents-sign-48148.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" alt="">
                        <div class="exp-cnt">
                            <span>
                                <img src="img/exp-icon-9.png" alt="">
                            </span>
                            <label>Third-Party Audit</label>
                            <p>We approach debt recovery issues without threatening clients’ existing business relations, time and money...</p>
                            <strong class="read-more-btn">View More <i class="ms-2 bi bi-arrow-right"></i></strong>
                        </div>
                    </a>

                    <a href="service-inner-9.php" class="single-exp-wrap">
                        <b></b>
                        <img src="https://images.pexels.com/photos/8730980/pexels-photo-8730980.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" alt="">
                        <div class="exp-cnt">
                            <span>
                                <img src="img/exp-icon-10.png" alt="">
                            </span>
                            <label>Trademark Registration</label>
                            <p>We approach debt recovery issues without threatening clients’ existing business relations, time and money...</p>
                            <strong class="read-more-btn">View More <i class="ms-2 bi bi-arrow-right"></i></strong>
                        </div>
                    </a>
                    
                </div>
            </div>
        </section>

        <section class="container-fluid">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 text-center my-4 my-md-5">
                        <h2>Our <span>3</span> Step Approach</h2>
                    </div>

                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="our-approch_wrap">
                            <span>
                                <div class="ring-me"></div>
                            </span>
                            <label>Ring Us</label>
                            <p>Get back to our friendly staff and share your concerns. We are the right team for your rights.</p>
                            <a href="#" class="read-more-btn">Contact Us <i class="ms-2 bi bi-arrow-right"></i></a>
                        </div>
                    </div>

                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="our-approch_wrap">
                            <span>
                                <div class="abt-me"></div>
                            </span>
                            <label>Learn more about us</label>
                            <p>Our expert team of professionals adhere to the legal needs of clients with our wide range of high-quality services.</p>
                            <a href="#" class="read-more-btn">Learn More <i class="ms-2 bi bi-arrow-right"></i></a>
                        </div>
                    </div>

                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="our-approch_wrap">
                            <span>
                                <div class="book-me"></div>
                            </span>
                            <label>Book an appointment</label>
                            <p>Let’s get started with our initial consultation tailored to your legal needs.</p>
                            <a href="#" class="read-more-btn">Book Now <i class="ms-2 bi bi-arrow-right"></i></a>
                        </div>
                    </div>

                </div>
            </div>
        </section>

        <section class="container-fluid team-wrap">
            <div class="container">
                <div class="row align-content-end">
                    <div class="col-12">
                        <div class="team-img-wrap">
                            <img src="img/our-team.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="container-fluid our-attorney-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <div class="meet-wrap">
                            <div class="brdr">
                                <h2>Meet Our Attorneys</h2>
                                <p class="text-center">A vibrant team that provides pre-eminent legal services and exceeds customer satisfaction. Schedule a discussion. Our legal consultants are available at your service.</p>
                                <div class="d-flex justify-content-center">
                                    <a href="#" class="fill-btn"><span>Practice Area</span></a>
                                    <a href="#" class="outline-btn"><span>Contact</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row py-2 py-md-5">

                    <div class="col-12 col-md-4">
                        <div class="satisfy-customer">
                            <span>
                                <img src="img/satisfy.svg" alt="">
                            </span>
                            <h2>200+</h2>
                            <label>Happy Customers</label>
                        </div>
                    </div>

                    <div class="col-12 col-md-4">
                        <div class="satisfy-customer">
                            <span>
                                <img src="img/satisfy-2.svg" alt="">
                            </span>
                            <h2>100%</h2>
                            <label>Success Rates</label>
                        </div>
                    </div>

                    <div class="col-12 col-md-4">
                        <div class="satisfy-customer">
                            <span>
                                <img src="img/satisfy-3.svg" alt="">
                            </span>
                            <h2>True service</h2>
                            <label>Our Magic Formula</label>
                        </div>
                    </div>

                </div>
            </div>
        </section>

        <section class="container-fluid news-events">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center my-3 my-md-5">
                        <h2>News Updates</h2>
                    </div>

                    <div class="col-12 col-md-4">
                        <div class="news-wrap">
                            <span>
                                <img src="https://images.unsplash.com/photo-1505488387362-48bc38155987?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1171&q=80" alt="">
                            </span>
                            <h3>we can achieve anything by heart</h3>
                            <div class="date-wrap">
                                <label>November 30, 2022</label>
                                <a href="#" class="read-more-btn">Read More <i class="ms-2 bi bi-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-4">
                        <div class="news-wrap">
                            <span>
                                <img src="https://images.unsplash.com/photo-1589391886645-d51941baf7fb?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80" alt="">
                            </span>
                            <h3>we can achieve anything by heart</h3>
                            <div class="date-wrap">
                                <label>November 30, 2022</label>
                                <a href="#" class="read-more-btn">Read More <i class="ms-2 bi bi-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-4">
                        <div class="news-wrap">
                            <span>
                                <img src="https://images.unsplash.com/photo-1589216532372-1c2a367900d9?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1171&q=80" alt="">
                            </span>
                            <h3>we can achieve anything by heart</h3>
                            <div class="date-wrap">
                                <label>November 30, 2022</label>
                                <a href="#" class="read-more-btn">Read More <i class="ms-2 bi bi-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 text-center my-3 my-md-5 d-flex justify-content-center">
                        <a href="#" class="read-more-btn d-flex align-items-center">Read More <i class="ms-2 bi bi-arrow-right"></i></a>
                    </div>

                </div>
            </div>
        </section>

        <section class="container-fluid our-attorney-wrap events">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center my-2 my-md-5">
                        <label class="mb-2">Explore Our <span>Events</span></label>
                        <h2>Selected Galleries</h2>
                    </div>
                </div>

                <div class="row justify-content-center">

                    <div class="col-12 col-md-4 p-0">
                        <a class="gall-card" href="#">
                            <span>
                                <img src="https://images.unsplash.com/photo-1472417583565-62e7bdeda490?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80" alt="">
                            </span>
                            <div class="cnt-wrap">
                                <h3>legal team of the year</h3>
                                <p class="text-center">Our expert team of professionals adhere to the legal needs of clients</p>
                                <div class="read-more-btn">View More <i class="ms-2 bi bi-arrow-right"></i></div>
                                <label>31 Photos</label>
                            </div>
                        </a>
                    </div>

                    <div class="col-12 col-md-4 p-0">
                        <a class="gall-card" href="#">
                            <span>
                                <img src="https://images.unsplash.com/photo-1511578314322-379afb476865?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1169&q=80" alt="">
                            </span>
                            <div class="cnt-wrap">
                                <h3>legal team of the year</h3>
                                <p class="text-center">Our expert team of professionals adhere to the legal needs of clients</p>
                                <div class="read-more-btn">View More <i class="ms-2 bi bi-arrow-right"></i></div>
                                <label>31 Photos</label>
                            </div>
                        </a>
                    </div>

                    <div class="col-12 col-md-4 p-0">
                        <a class="gall-card" href="#">
                            <span>
                                <img src="https://images.unsplash.com/photo-1459499362902-55a20553e082?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1169&q=80" alt="">
                            </span>
                            <div class="cnt-wrap">
                                <h3>legal team of the year</h3>
                                <p class="text-center">Our expert team of professionals adhere to the legal needs of clients</p>
                                <div class="read-more-btn">View More <i class="ms-2 bi bi-arrow-right"></i></div>
                                <label>31 Photos</label>
                            </div>
                        </a>
                    </div>

                    <div class="col-12 text-center my-3 my-md-5 d-flex justify-content-end">
                        <a href="gallery-listing.php" class="read-more-btn d-flex align-items-center">View More <i class="ms-2 bi bi-arrow-right"></i></a>
                    </div>

                </div>

            </div>
        </section>

        <?php include 'footer.php';?>
<?php include 'header.php';?>

<!------------------------------------------------------------------edititing start here-------------------------------------------------->


<section class="container-fluid inner-banner">
    <div class="row align-items-center text-center">
        <div class="inner-banner-img-wrap">
            <img src="https://images.unsplash.com/photo-1473186505569-9c61870c11f9?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80" alt="">
        </div>
        <h2>Corporate Services</h2>
    </div>
</section>

<section class="container-fluid serv-inner-sec">
    <div class="container">
        <div class="row">

            <div class="col-12 col-md-6 col-lg-8">
                <div class="serv-img-wrap">
                    <img src= "https://images.unsplash.com/photo-1454165804606-c3d57bc86b40?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2070&q=80" alt="">
                </div>
                
                <div class="mb-4">
                    <h3>Service that matters!!</h3>
                    <p> GLC is globally recognized in serving corporate and commercial clients from education, healthcare, hospitality, technology, insurance & more. 
                    Our in-depth skill set in legal consultation has successfully supported domestic as well as international clients. Let us serve you the best in mergers & acquisitions, capital dealing etc. </p>
                </div>
                
                <div class="mb-4">
                    <h3>Service is truly different…</h3>
                    <p>Ready to maximize your profit? Our corporate service team ensures every aspect of your business objectives is met, improves ROI, and compliance with regulatory requirements by proper administration of corporate records.
                    As a market leader in the corporate service industry, we have strategies for serving things right. Our highly qualified legal experts serve you with global solutions beyond your expectation.</p>
                </div>
                
                <div class="mb-4">
                    <h3>Let us serve you in: </h3>
                    <ul>
                        <li>Assisting clients in capital dealings in the industry</li>
                        <li>Guiding clients in the corporate finance sector</li>
                        <li>Standing out from competitors</li>
                        <li>Advising on mergers and acquisitions</li>
                        <li>Maximize ROI</li>
                        <li>Assisting foreign direct investment projects</li>
                        <li>Documentation and other drafting of agreements or contracts</li>
                    </ul>
                </div>
                
            </div>
            
            <!------------------------------------------------------------------edititing end here-------------------------------------------------->

            <div class="col-12 col-md-6 col-lg-4">
                <div class="practice-area">
                    <h4>Practice Area</h4>
                    <div class="">
                        <a href="#" class="active">ligitation <i class="bi bi-chevron-right"></i></a>
                        <a href="#">Arbitration <i class="bi bi-chevron-right"></i></a>
                        <a href="#">Debt Recovery <i class="bi bi-chevron-right"></i></a>
                        <a href="#">Legal Consultation<i class="bi bi-chevron-right"></i></a>
                        <a href="#">Company Setup <i class="bi bi-chevron-right"></i></a>
                        <a href="#">Corporate Services <i class="bi bi-chevron-right"></i></a>
                        <a href="#">Company Representation <i class="bi bi-chevron-right"></i></a>
                        <a href="#">PRO Services<i class="bi bi-chevron-right"></i></a>
                        <a href="#">Third-Party Audit <i class="bi bi-chevron-right"></i></a>
                        <a href="#">Trademark Registration <i class="bi bi-chevron-right"></i></a>
                    </div>

                </div>

                <div class="practice-area brochure">
                    <h4>our brochure</h4>
                    <div class="bro-div">
                        <p>At GLC, we offer affordable smart legal solutions serving local as well as overseas clients. We understand your concern and use our professionalism</p>
                    </div>
                    <a href="#" class="read-more-btn"><i class="bi bi-file-earmark-text me-2"></i>Download Brochure</a>
                </div>


            </div>

        </div>
    </div>
</section>

<section class="container-fluid contact_ad ad_1">
    <div class="row align-items-center">
        <div class="col-12 p-0">
            <div class="">
                <h2>are you looking for a consultation ?</h1>
                <p>Our expert team of professionals adhere to the legal needs of clients with our wide range of high-quality services.</p>
                <div class="d-flex justify-content-center">
                    <a href="#" class="fill-btn"><span>Practice Area</span></a>
                    <a href="#" class="outline-btn"><span>Contact</span></a>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>
<?php include 'header.php';?>

<!------------------------------------------------------------------edititing start here-------------------------------------------------->


<section class="container-fluid inner-banner">
    <div class="row align-items-center text-center">
        <div class="inner-banner-img-wrap">
            <img src="https://images.unsplash.com/photo-1473186505569-9c61870c11f9?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80" alt="">
        </div>
        <h2>Legal Consultation</h2>
    </div>
</section>

<section class="container-fluid serv-inner-sec">
    <div class="container">
        <div class="row">

            <div class="col-12 col-md-6 col-lg-8">
                <div class="serv-img-wrap">
                    <img src= "https://images.pexels.com/photos/5668481/pexels-photo-5668481.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" alt="">
                </div>
                
                <div class="mb-4">
                    <h3>For the perfect legal solutions…</h3>
                    <p>GLC is an award-winning legal consulting firm with incredible lawyers who pursue cutting-edge approaches to legal issues. 
                    Clients are our highest priority! We adopt modern techniques and innovation in providing legal services with the highest standard of quality. 
                    We conduct thorough research on client issues, gather evidence and ensure confidentiality in information.</p>
                </div>
                
                <div class="mb-4">
                    <h3>Resolve your legal disputes </h3>
                    <p>We are here to help you with a smart professional opinion. Let us assist those who are unable to access the court at ease.
                    We, the entire team of GLC, are proud of the industry expertise and proficiency in handling legal affairs thereby protecting client interests. 
                    Are you looking forward to legal consultation with international standards? Get professional guidance in monetary compensation, criminal, civil & property,
                    family & matrimony advice, banking & corporate matters, and employment & labour matters. </p>
                </div>
                </div>
                
            <!------------------------------------------------------------------edititing end here-------------------------------------------------->

         <div class="col-12 col-md-6 col-lg-4">
                <div class="practice-area">
                    <h4>Practice Area</h4>
                    <div class="">
                        <a href="#" class="active">ligitation <i class="bi bi-chevron-right"></i></a>
                        <a href="#">Arbitration <i class="bi bi-chevron-right"></i></a>
                        <a href="#">Debt Recovery <i class="bi bi-chevron-right"></i></a>
                        <a href="#">Legal Consultation<i class="bi bi-chevron-right"></i></a>
                        <a href="#">Company Setup <i class="bi bi-chevron-right"></i></a>
                        <a href="#">Corporate Services <i class="bi bi-chevron-right"></i></a>
                        <a href="#">Company Representation <i class="bi bi-chevron-right"></i></a>
                        <a href="#">PRO Services<i class="bi bi-chevron-right"></i></a>
                        <a href="#">Third-Party Audit <i class="bi bi-chevron-right"></i></a>
                        <a href="#">Trademark Registration <i class="bi bi-chevron-right"></i></a>
                    </div>

                </div>

                <div class="practice-area brochure">
                    <h4>our brochure</h4>
                    <div class="bro-div">
                        <p>At GLC, we offer affordable smart legal solutions serving local as well as overseas clients. We understand your concern and use our professionalism</p>
                    </div>
                    <a href="#" class="read-more-btn"><i class="bi bi-file-earmark-text me-2"></i>Download Brochure</a>
                </div>


            </div>

        </div>
    </div>
</section>

<section class="container-fluid contact_ad ad_1">
    <div class="row align-items-center">
        <div class="col-12 p-0">
            <div class="">
                <h2>are you looking for a consultation ?</h1>
                <p>Our expert team of professionals adhere to the legal needs of clients with our wide range of high-quality services.</p>
                <div class="d-flex justify-content-center">
                    <a href="#" class="fill-btn"><span>Practice Area</span></a>
                    <a href="#" class="outline-btn"><span>Contact</span></a>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>
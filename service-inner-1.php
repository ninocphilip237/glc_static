<?php include 'header.php';?>

<!------------------------------------------------------------------edititing start here-------------------------------------------------->


<section class="container-fluid inner-banner">
    <div class="row align-items-center text-center">
        <div class="inner-banner-img-wrap">
            <img src="https://images.unsplash.com/photo-1473186505569-9c61870c11f9?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80" alt="">
        </div>
        <h2>Litigation</h2>
    </div>
</section>

<section class="container-fluid serv-inner-sec">
    <div class="container">
        <div class="row">

            <div class="col-12 col-md-6 col-lg-8">
                <div class="serv-img-wrap">
                    <img src="https://images.unsplash.com/photo-1502465771179-51f3535da42c?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1169&q=80" alt="">
                </div>
                
                <div class="mb-4">
                    <h3>Our Experience is Your Solution!!</h3>
                    <p>Your justice is our motto. You are not just a client, rather we are your legal friend. 
                        GLC is a pioneer in smart legal services and delivers expert solutions from the most skilled,
                         professional and international legal consultants. We understand your concern and provide full-service
                          Litigation in Abu Dhabi.</p>
                </div>
                
                <div class="mb-4">
                    <h3>Choose the right team for your rights…</h3>
                    <p>When a client approaches, we appreciate their trust and the time spent. It’s quite time-consuming in defending a claim. However, we initially suggest a practical way to resolve the matter but nevertheless insist you compromise besides your interests.
                        <br>Our words & actions matter the most. Our expertise and confidence provide appropriate advice in taking the right decision at the right time.
                        <br>Let us deliver inventive guidance and advice solutions that reduce your legal costs. Just count on us. Our local, international and regional expertise speaks for you.</p>
                </div>
                
                <div class="mb-4">
                    <h3>Our highly qualified team of lawyers are experts in</h3>
                    <ul>
                        <li>Commercial Litigation</li>
                        <li>Civil Litigation</li>
                        <li>Criminal Litigation</li>
                    </ul>
                </div>
                
            </div>
            
            <!------------------------------------------------------------------edititing end here-------------------------------------------------->

            <div class="col-12 col-md-6 col-lg-4">
                <div class="practice-area">
                    <h4>Practice Area</h4>
                    <div class="">
                        <a href="#" class="active">ligitation <i class="bi bi-chevron-right"></i></a>
                        <a href="#">Arbitration <i class="bi bi-chevron-right"></i></a>
                        <a href="#">Debt Recovery <i class="bi bi-chevron-right"></i></a>
                        <a href="#">Legal Consultation<i class="bi bi-chevron-right"></i></a>
                        <a href="#">Company Setup <i class="bi bi-chevron-right"></i></a>
                        <a href="#">Corporate Services <i class="bi bi-chevron-right"></i></a>
                        <a href="#">Company Representation <i class="bi bi-chevron-right"></i></a>
                        <a href="#">PRO Services<i class="bi bi-chevron-right"></i></a>
                        <a href="#">Third-Party Audit <i class="bi bi-chevron-right"></i></a>
                        <a href="#">Trademark Registration <i class="bi bi-chevron-right"></i></a>
                    </div>

                </div>

                <div class="practice-area brochure">
                    <h4>our brochure</h4>
                    <div class="bro-div">
                        <p>At GLC, we offer affordable smart legal solutions serving local as well as overseas clients. We understand your concern and use our professionalism</p>
                    </div>
                    <a href="#" class="read-more-btn"><i class="bi bi-file-earmark-text me-2"></i>Download Brochure</a>
                </div>


            </div>

        </div>
    </div>
</section>

<section class="container-fluid contact_ad ad_1">
    <div class="row align-items-center">
        <div class="col-12 p-0">
            <div class="">
                <h2>are you looking for a consultation ?</h1>
                <p>Our expert team of professionals adhere to the legal needs of clients with our wide range of high-quality services.</p>
                <div class="d-flex justify-content-center">
                    <a href="#" class="fill-btn"><span>Practice Area</span></a>
                    <a href="#" class="outline-btn"><span>Contact</span></a>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>
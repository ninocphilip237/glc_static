<?php include 'header.php';?>

<!------------------------------------------------------------------edititing start here-------------------------------------------------->


<section class="container-fluid inner-banner">
    <div class="row align-items-center text-center">
        <div class="inner-banner-img-wrap">
            <img src="https://images.unsplash.com/photo-1473186505569-9c61870c11f9?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80" alt="">
        </div>
        <h2>PRO Services</h2>
    </div>
</section>

<section class="container-fluid serv-inner-sec">
    <div class="container">
        <div class="row">

            <div class="col-12 col-md-6 col-lg-8">
                <div class="serv-img-wrap">
                    <img src= "https://images.pexels.com/photos/5669602/pexels-photo-5669602.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" alt="">
                </div>
                
                <div class="mb-4">
                    <h3>Our Vision Is Our Best Service</h3>
                    <p> GLC is specialized in professional services to deliver unparalleled services at the local and international levels. 
                    Do you know what's the most challenging aspect of running a profitable business? Choosing the right service with the right people. We serve differently with innovation at every step. </p>
                </div>
                
                <div class="mb-4">
                    <h3>GLC- With the professional team! </h3>
                    <p> Looking forward to driving your company with innovative solutions? Let us tackle your regulatory hassles through a balanced perspective. </p>
                    <ul>
                        <li>Are you looking forward to annually renewing trade license transactions? Our expert PRO service provider handles the situation at ease and issues a modified trade license.</li>
                        <li>We ensure smooth employment visa transactions with minimal effort.</li>
                        <li>Our expertise in various jurisdictions saves your commutation time & cost by serving logistic services, for external Government approvals etc.</li>
                    </ul>
                </div>
                
            </div>
            
            <!------------------------------------------------------------------edititing end here-------------------------------------------------->

            <div class="col-12 col-md-6 col-lg-4">
                <div class="practice-area">
                    <h4>Practice Area</h4>
                    <div class="">
                        <a href="#" class="active">ligitation <i class="bi bi-chevron-right"></i></a>
                        <a href="#">Arbitration <i class="bi bi-chevron-right"></i></a>
                        <a href="#">Debt Recovery <i class="bi bi-chevron-right"></i></a>
                        <a href="#">Legal Consultation<i class="bi bi-chevron-right"></i></a>
                        <a href="#">Company Setup <i class="bi bi-chevron-right"></i></a>
                        <a href="#">Corporate Services <i class="bi bi-chevron-right"></i></a>
                        <a href="#">Company Representation <i class="bi bi-chevron-right"></i></a>
                        <a href="#">PRO Services<i class="bi bi-chevron-right"></i></a>
                        <a href="#">Third-Party Audit <i class="bi bi-chevron-right"></i></a>
                        <a href="#">Trademark Registration <i class="bi bi-chevron-right"></i></a>
                    </div>

                </div>

                <div class="practice-area brochure">
                    <h4>our brochure</h4>
                    <div class="bro-div">
                        <p>At GLC, we offer affordable smart legal solutions serving local as well as overseas clients. We understand your concern and use our professionalism</p>
                    </div>
                    <a href="#" class="read-more-btn"><i class="bi bi-file-earmark-text me-2"></i>Download Brochure</a>
                </div>


            </div>

        </div>
    </div>
</section>

<section class="container-fluid contact_ad ad_1">
    <div class="row align-items-center">
        <div class="col-12 p-0">
            <div class="">
                <h2>are you looking for a consultation ?</h1>
                <p>Our expert team of professionals adhere to the legal needs of clients with our wide range of high-quality services.</p>
                <div class="d-flex justify-content-center">
                    <a href="#" class="fill-btn"><span>Practice Area</span></a>
                    <a href="#" class="outline-btn"><span>Contact</span></a>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>
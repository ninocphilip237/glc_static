<?php include 'header.php';?>


<section class="container-fluid inner-banner">
    <div class="row align-items-center text-center">
        <div class="inner-banner-img-wrap">
            <img src="https://images.unsplash.com/photo-1511578314322-379afb476865?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1169&q=80" alt="">
        </div>
        <h2>Gallery</h2>
    </div>
</section>

<section class="container-fluid my-5">
    <div class="container">
        <div class="row justify-content-center">

            <div class="col-12 col-md-4 mb-4">
                <a class="gall-card" href="#">
                    <span>
                        <img src="https://images.unsplash.com/photo-1472417583565-62e7bdeda490?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80" alt="">
                    </span>
                    <div class="cnt-wrap">
                        <h3>legal team of the year</h3>
                        <p class="text-center">Our expert team of professionals adhere to the legal needs of clients</p>
                        <div class="read-more-btn">View More <i class="ms-2 bi bi-arrow-right"></i></div>
                        <label>31 Photos</label>
                    </div>
                </a>
            </div>

            <div class="col-12 col-md-4 mb-4">
                <a class="gall-card" href="#">
                    <span>
                        <img src="https://images.unsplash.com/photo-1511578314322-379afb476865?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1169&q=80" alt="">
                    </span>
                    <div class="cnt-wrap">
                        <h3>legal team of the year</h3>
                        <p class="text-center">Our expert team of professionals adhere to the legal needs of clients</p>
                        <div class="read-more-btn">View More <i class="ms-2 bi bi-arrow-right"></i></div>
                        <label>31 Photos</label>
                    </div>
                </a>
            </div>

            <div class="col-12 col-md-4 mb-4">
                <a class="gall-card" href="#">
                    <span>
                        <img src="https://images.unsplash.com/photo-1459499362902-55a20553e082?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1169&q=80" alt="">
                    </span>
                    <div class="cnt-wrap">
                        <h3>legal team of the year</h3>
                        <p class="text-center">Our expert team of professionals adhere to the legal needs of clients</p>
                        <div class="read-more-btn">View More <i class="ms-2 bi bi-arrow-right"></i></div>
                        <label>31 Photos</label>
                    </div>
                </a>
            </div>
            
            <div class="col-12 col-md-4 mb-4">
                <a class="gall-card" href="#">
                    <span>
                        <img src="https://images.unsplash.com/photo-1459499362902-55a20553e082?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1169&q=80" alt="">
                    </span>
                    <div class="cnt-wrap">
                        <h3>legal team of the year</h3>
                        <p class="text-center">Our expert team of professionals adhere to the legal needs of clients</p>
                        <div class="read-more-btn">View More <i class="ms-2 bi bi-arrow-right"></i></div>
                        <label>31 Photos</label>
                    </div>
                </a>
            </div>
            
            <div class="col-12 col-md-4 mb-4">
                <a class="gall-card" href="#">
                    <span>
                        <img src="https://images.unsplash.com/photo-1459499362902-55a20553e082?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1169&q=80" alt="">
                    </span>
                    <div class="cnt-wrap">
                        <h3>legal team of the year</h3>
                        <p class="text-center">Our expert team of professionals adhere to the legal needs of clients</p>
                        <div class="read-more-btn">View More <i class="ms-2 bi bi-arrow-right"></i></div>
                        <label>31 Photos</label>
                    </div>
                </a>
            </div>
            
            <div class="col-12 col-md-4 mb-4">
                <a class="gall-card" href="#">
                    <span>
                        <img src="https://images.unsplash.com/photo-1459499362902-55a20553e082?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1169&q=80" alt="">
                    </span>
                    <div class="cnt-wrap">
                        <h3>legal team of the year</h3>
                        <p class="text-center">Our expert team of professionals adhere to the legal needs of clients</p>
                        <div class="read-more-btn">View More <i class="ms-2 bi bi-arrow-right"></i></div>
                        <label>31 Photos</label>
                    </div>
                </a>
            </div>

            <div class="col-12 text-center my-3 my-md-5 d-flex justify-content-center">
                <a href="#" class="read-more-btn d-flex align-items-center">Load More <i class="ms-2 bi bi-arrow-right"></i></a>
            </div>

        </div>

    </div>
</section>

<?php include 'header.php';?>